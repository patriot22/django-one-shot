from django.urls import path
from todos.views import show_lists

urlpatterns = [
    path("todos/", show_lists, name = "show_lists")
]
